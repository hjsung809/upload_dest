const electron = require('electron')
const { ipcMain } = require('electron');
const{ app , BrowserWindow } = require('electron')

const fs = require('fs')
const path = require('path')
const readline = require('readline');


let win;

function createWindow(){
	win = new BrowserWindow(
  {
    webPreferences: {
        nodeIntegration: true
    },
		width: 1450,
		height: 1050 }
  );

	win.loadFile('./index.html')
	// win.webContents.openDevTools()
  win.webContents.openDevTools()
  win.maximize();

	win.setTitle("UPLOAD DEST");

  //
	// // var fd = fs.openSync('./PIPE_DATA','r+')
	// var pipe_data_stream = fs.createReadStream('./PIPE_DATA',{
	// 	flags:"r+"
	// 	,autoClose: true
	// });
  //
	// var rl = readline.createInterface({
	// 	input: pipe_data_stream
	// 	// ,output: process.stdout
	// });
	// rl.on('line', (data) => {
	// 	console.log('got from pipe:' + data);
	// 	// win.webContents.send('data_receive', data);
	// });



  win.on('closed',()=>{
  	 console.log('win.on closed');
		 // pipe_data_stream.destroy();
		 // rl.close();
	   // app.quit()
	   win=null;
  });
}

app.on('ready',createWindow)

app.on('window-all-closed', () => {
	app.quit();
  console.log('app.on window-all-closed');
});

app.on('activate',()=>{
  console.log('app.on activate');
	if(win ===null){
	 createWindow();
     console.log('win is null. createWindow!');
	}
});
